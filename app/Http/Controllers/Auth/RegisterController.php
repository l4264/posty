<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller {
    public function index() {
        $inputs = [
            ['name'=>'name', 'for'=>'name', 'label'=>'Name', 'type'=>'text'],
            ['name'=>'username', 'for'=>'username', 'label'=>'Username', 'type'=>'text'],
            ['name'=>'email', 'for'=>'email', 'label'=>'Email', 'type'=>'email'],
            ['name'=>'password', 'for'=>'password', 'label'=>'Password', 'type'=>'password'],
            ['name'=>'password', 'for'=>'password_confirmation', 'label'=>'Password again', 'type'=>'password'],
        ];
        return view('auth.register', ['inputs'=>$inputs]);
    }

    public function store(Request $request) {
//        log request object
//        dd($request);
//        dd($request->get('email'));
        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
//            'password' => 'required|confirmed',
        ]);
    }
}

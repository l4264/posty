<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet" >
</head>
<body class="bg-gray-100">
<nav class="p-6 bg-white flex justify-between mb-6">
    <ul class="flex items-center">
        <li>
            <a href="/" class="p-3">Home</a>
        </li>
    </ul>

    <ul class="flex items-center">
        <li>
            <a href="" class="p-3">@Brogrammer.hide</a>
        </li>
        <li>
            <a href="" class="p-3">Login</a>
        </li>
        <li>
            <a href="{{ route('register') }}" class="p-3">Register</a>
        </li>
        <li>
            <a href="" class="p-3">Logout</a>
        </li>
    </ul>
</nav>
@yield('content')
</body>
</html>

@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-4/12 bg-white p-6 rounded-lg">
            <form action="{{ route('register') }}" method="post">
                @csrf
                @foreach($inputs as $input)
                    <div class="mb-4">
{{--                        <label for="{{ $input['for'] }}">{{ $input['label'] }}</label>--}}
                        <input
                            id="{{ $input['for'] }}"
                            type="{{ $input['type'] }}"
                            name="{{ $input['name'] }}"
                            placeholder="{{ $input['label']  }}"
                            class="bg-gray-100 border-2 w-full rounded-lg p-4"
                            value=""
                        >
                    </div>
                @endforeach
                    <div>
                        <button type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full">Register</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
